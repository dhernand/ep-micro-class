#include <R.h>
#include <Rinternals.h>
#include <Rdefines.h>
#include <Rmath.h>
#include <R_ext/Rdynload.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double approximatePartitionFunctionEP(int d, double *N, int sn, double alpha, double beta, SEXP R_runif, SEXP R_rho);

/* Structure with information for the approximate term that corresponds to the first d terms of the Markov random field prior */

typedef struct {
	double *cTilde;
	double *dTilde;
	double sTilde;
} FirstTermMarkov;

/* Structure with information for the approximate term that corresponds to the second terms of the Markov random field prior */

typedef struct {
	double *c1Tilde;
	double *d1Tilde;
	double *c2Tilde;
	double *d2Tilde;
	double *sTilde;
} SecondTermMarkov;

/* Structure with information for the approximate term that corresponds to the spike and lab prior */

typedef struct {
	double *cTilde;
	double *dTilde;
	double *mTilde;
	double *vTilde;
	double sTilde;
} TermSpikeAndSlab;

/* Structure with information for the approximate term that corresponds to the likelihood terms */

typedef struct {
	double *mTilde;
	double *vTilde;
	double aTilde;
	double bTilde;
	double sTilde;
} TermLikelihood;

/* Structure with information for the approximate term that corresponds to prior for epsilon */

typedef struct {
	double aTilde;
	double bTilde;
	double sTilde;
} TermPriorEpsilon;
	
/**
 * Function that computes the absolute value
 */

double internalAbsoluteValue(double x) {

	if (x < 0)
		return -x;
	else
		return x;
}

/**
 * Function that comptues the maximum
 *
 */

double max(double a, double b) {

	if (a > b)
		return a;
	else
		return b;
}

/* Function that solves the system:
 *
 * digamma(a) - digamma(a + b) = K1
 * digamma(b) - digamma(a + b) = K2
 *
 */

void findAnewBnew(double K1, double K2, double *a, double *b, SEXP optimizer, SEXP rho) {

	SEXP K1prima, K2prima, R_a, R_b, ret;

	PROTECT(K1prima = NEW_NUMERIC(1));
	PROTECT(K2prima = NEW_NUMERIC(1));

	PROTECT(R_a = NEW_NUMERIC(1));
	PROTECT(R_b = NEW_NUMERIC(1));

	REAL(R_a)[0] = *a;
	REAL(R_b)[0] = *b;

	REAL(K1prima)[0] = K1;
	REAL(K2prima)[0] = K2;

	setVar(install("K1"), K1prima, rho);
	setVar(install("K2"), K2prima, rho);

	setVar(install("a"), R_a, rho);
	setVar(install("b"), R_b, rho);

	PROTECT(ret = eval(optimizer, rho));

	*a = REAL(ret)[ 0 ];
	*b = REAL(ret)[ 1 ];

	UNPROTECT(5);
}

/**
 * Function that computes a permutation of the vector 0 : (n - 1)
 *
 */

int *computePermutation(int n, SEXP runif, SEXP rho) {

	int i, j, k;
	int *ret;

	SEXP random;

	ret = (int *) malloc(sizeof(int) * n);

	for (i = 0 ; i < n ; i++)
		ret[ i ] = i;

	for (i = 0 ; i < n ; i++) {

		PROTECT(random = eval(runif, rho));
		j = round(*REAL(random) * (n - 1));
		UNPROTECT(1);

		k = ret[ i ];
		ret[ i ] = ret[ j ];
		ret[ j ] = k;
	}

	return ret;
}

/**
 * Function that initializes the approximate terms
 */

void initializeApproximateTerms(FirstTermMarkov *firstTermMarkov, SecondTermMarkov *secondTermMarkov, TermSpikeAndSlab *termSpikeAndSlab,
	TermLikelihood **termLikelihood, TermPriorEpsilon *termPriorEpsilon, int d, int sn, int st, double alpha) {

	int i, j;

	/* We initialize the approximate term for the first d terms of the Markov random field prior */

	firstTermMarkov->cTilde = (double *) malloc(sizeof(double) * d);
	firstTermMarkov->dTilde = (double *) malloc(sizeof(double) * d);
	firstTermMarkov->sTilde = 0;
	for (i = 0 ; i < d ; i++) {
/*		firstTermMarkov->cTilde[ i ] = 1;
		firstTermMarkov->dTilde[ i ] = 1;*/
		firstTermMarkov->cTilde[ i ] = exp(alpha) / (exp(alpha) + exp(-alpha));
		firstTermMarkov->dTilde[ i ] = 1 - exp(alpha) / (exp(alpha) + exp(-alpha));
	}

	/* We initialize the approximate term for the second term of the Markov random field prior */

	secondTermMarkov->c1Tilde = (double *) malloc(sizeof(double) * sn);
	secondTermMarkov->d1Tilde = (double *) malloc(sizeof(double) * sn);
	secondTermMarkov->c2Tilde = (double *) malloc(sizeof(double) * sn);
	secondTermMarkov->d2Tilde = (double *) malloc(sizeof(double) * sn);
	secondTermMarkov->sTilde = (double *) malloc(sizeof(double) * sn);
	for (i = 0 ; i < sn ; i++) {
		secondTermMarkov->c1Tilde[ i ] = 1;
		secondTermMarkov->d1Tilde[ i ] = 1;
		secondTermMarkov->c2Tilde[ i ] = 1;
		secondTermMarkov->d2Tilde[ i ] = 1;
		secondTermMarkov->sTilde[ i ] = 0;
	}

	/* We initialize the approximate term for the spike and slab prior */

	termSpikeAndSlab->cTilde = (double *) malloc(sizeof(double) * d);
	termSpikeAndSlab->dTilde = (double *) malloc(sizeof(double) * d);
	termSpikeAndSlab->mTilde = (double *) malloc(sizeof(double) * d);
	termSpikeAndSlab->vTilde = (double *) malloc(sizeof(double) * d);
	for (i = 0 ; i < d ; i++) {
		termSpikeAndSlab->cTilde[ i ] = 1;
		termSpikeAndSlab->dTilde[ i ] = 1;
		termSpikeAndSlab->mTilde[ i ] = 0;
/*		termSpikeAndSlab->vTilde[ i ] = HUGE_VAL;*/
		termSpikeAndSlab->vTilde[ i ] = exp(alpha) / (exp(alpha) + exp(-alpha)) * 1;
	}
	termSpikeAndSlab->sTilde = 0;

	/* We initialize the approximate terms for the likelihood */

	*termLikelihood = (TermLikelihood *) malloc(sizeof(TermLikelihood) * st);
	for (i = 0 ; i < st ; i++) {
		(*termLikelihood)[ i ].mTilde = (double *) malloc(sizeof(double) * d);
		(*termLikelihood)[ i ].vTilde = (double *) malloc(sizeof(double) * d);
		for (j = 0 ; j < d ; j++) {
			(*termLikelihood)[ i ].mTilde[ j ] = 0;
			(*termLikelihood)[ i ].vTilde[ j ] = HUGE_VAL;
		}
		(*termLikelihood)[ i ].aTilde = 0;
		(*termLikelihood)[ i ].bTilde = 0;
		(*termLikelihood)[ i ].sTilde = 0;
	}

	termPriorEpsilon->aTilde = 0;
	termPriorEpsilon->bTilde = 0;
	termPriorEpsilon->sTilde = 0;
}

/**
 * Function that computes the posterior approximation by EP in the network based sparse Bayesian classifier.
 *
 * X : feature matrix, one row per instance and one column by feature.
 * y : target labels.
 * st : size of the training set.
 * d : number of features.
 * N : Representation of the feature network. This is a 3 column matrix with one row per edge in the network with the format (node1, weight, node2)
 * sn : size of the feature network, that is, the number of edges in the network.
 * alpha : Hyperparameter for the Markov random field prior.
 * beta : Hyperparameter for the Markov random field prior.
 * a0 : Hyperparameter for the prior for epsilon.
 * b0 : Hyperparameter for the prior for epsilon.
 *
 * Returns:
 *
 * m : Mean vector of the Gaussian approximation for the model coefficients.
 * v : Variance vector of the Gaussian approximation for the model coefficients.
 * p : Probability vector of the Bernoulli approximation for the latent variables z.
 * a,b : Parameters of the Beta approximation for the noise level.
 * logEvidence : Estimate of the logarithm of the evidence.
 *
 */

SEXP nbsbc(SEXP R_X, SEXP R_y, SEXP R_st, SEXP R_d, SEXP R_N, SEXP R_sn, SEXP R_alpha, SEXP R_beta, SEXP R_a0, SEXP R_b0, SEXP R_optimizer,
	SEXP R_runif, SEXP R_rho) {

	SEXP R_m, R_v, R_p, R_a, R_b, R_logEvidence, R_ret, R_namesList, R_error;

	double alpha, beta, a0, b0, a, b, aOld, bOld, aBackup, bBackup,
		Z1, Z2, Z3, Z4, w, K1, K2, K3, K4, Z, Cprima, Cprimaprima,
		squareRoot, eHatOld, Phi, lambda, z, maxdif, logEvidence, A, B, D, C, v1;
	
	double register pOld_l, pOld_j;

	double *mBackup, *vBackup, *pBackup, *mOld, *vOld, *pOld, *m, *v, *p, *N, *X, *y, *cProduct, *dProduct;

	int i, st, sn, d, j, k, l, iterations, convergence, negativeVariance, h;

	int *indexSecondTermMarkov;

	FirstTermMarkov firstTermMarkov;
	SecondTermMarkov secondTermMarkov;
	TermSpikeAndSlab termSpikeAndSlab;
	TermPriorEpsilon termPriorEpsilon;
	TermLikelihood *termLikelihood;

	PROTECT(R_X = AS_NUMERIC(R_X));
	PROTECT(R_y = AS_NUMERIC(R_y));
	PROTECT(R_st = AS_INTEGER(R_st));
	PROTECT(R_d = AS_INTEGER(R_d));
	PROTECT(R_sn = AS_INTEGER(R_sn));
	PROTECT(R_N = AS_NUMERIC(R_N));
	PROTECT(R_alpha = AS_NUMERIC(R_alpha));
	PROTECT(R_beta = AS_NUMERIC(R_beta));
	PROTECT(R_a0 = AS_NUMERIC(R_a0));
	PROTECT(R_b0 = AS_NUMERIC(R_b0));

	alpha = *NUMERIC_POINTER(R_alpha);
	beta = *NUMERIC_POINTER(R_beta);

	a0 = *NUMERIC_POINTER(R_a0);
	b0 = *NUMERIC_POINTER(R_b0);
	st = *INTEGER_POINTER(R_st);
	sn = *INTEGER_POINTER(R_sn);
	d  = *INTEGER_POINTER(R_d);

	PROTECT(R_m = NEW_NUMERIC(d));
	PROTECT(R_v = NEW_NUMERIC(d));
	PROTECT(R_p = NEW_NUMERIC(d));
	PROTECT(R_a = NEW_NUMERIC(1));
	PROTECT(R_b = NEW_NUMERIC(1));
	PROTECT(R_logEvidence = NEW_NUMERIC(1));
	PROTECT(R_error = NEW_NUMERIC(1));
	PROTECT(R_ret = NEW_LIST(7));

	m = NUMERIC_POINTER(R_m);
	v = NUMERIC_POINTER(R_v);
	p = NUMERIC_POINTER(R_p);
	N = NUMERIC_POINTER(R_N);
	X = NUMERIC_POINTER(R_X);
	y = NUMERIC_POINTER(R_y);

	mBackup = (double *) malloc(sizeof(double) * d);
	vBackup = (double *) malloc(sizeof(double) * d);
	pBackup = (double *) malloc(sizeof(double) * d);

	mOld = (double *) malloc(sizeof(double) * d);
	vOld = (double *) malloc(sizeof(double) * d);
	pOld = (double *) malloc(sizeof(double) * d);

	/* We initialize the approximate terms */

	initializeApproximateTerms(&firstTermMarkov, &secondTermMarkov, &termSpikeAndSlab, &termLikelihood, &termPriorEpsilon, d, sn, st, alpha);

	/* We initialize the posterior approximation to be uniform */

	a = 1;
	b = 1;
	for (i = 0 ; i < d ; i++) {
/*		p[ i ] = 0.5;*/
		p[ i ] = exp(alpha) / (exp(alpha) + exp(-alpha));
		m[ i ] = 0;
/*		v[ i ] = HUGE_VAL;*/
		v[ i ] = exp(alpha) / (exp(alpha) + exp(-alpha)) * 1;
	}

	indexSecondTermMarkov = computePermutation(sn, R_runif, R_rho);

	/* Main loop of the algorithm */

	convergence = 0;
	iterations = 0;
	while (!convergence && iterations < 100) {

		/* We process the prior for epsilon */

/*		aOld = a - termPriorEpsilon.aTilde;
		bOld = b - termPriorEpsilon.bTilde;
		a = a0 + aOld - 1;
		b = b0 + bOld - 1;
		termPriorEpsilon.aTilde = a - aOld;
		termPriorEpsilon.bTilde = b - bOld;
		termPriorEpsilon.sTilde = -lbeta(a0, b0);*/

		/* We process the terms for the likelihood */

		for (i = 0; i < st ; i++) {
			Z = squareRoot = 0;
			negativeVariance = 0;
			for (j = 0 ; j < d ; j++) {
				vOld[ j ] = 1 / (1 / v[ j ] - 1 / termLikelihood[ i ].vTilde[ j ]);
				if (vOld[ j ] < 0)
					negativeVariance = 1;
				mOld[ j ] = m[ j ] + vOld[ j ] * 1 / termLikelihood[ i ].vTilde[ j ] * (m[ j ] - termLikelihood[ i ].mTilde[ j ]);
				squareRoot += vOld[ j ] * X[ i + st * j ] * X[ i + st * j ];
				Z += X[ i + st * j ] * mOld[ j ];
			}
			if (negativeVariance) {
			/*	printf("*"); fflush(stdout); */
				break;
			}

			squareRoot = sqrt(squareRoot + 1);
			Z = y[ i ] * Z  / squareRoot;
			Phi = pnorm(Z, 0, 1, 1, 0);
			lambda = 1 / squareRoot * 1 / sqrt(2 * PI) * exp(-0.5 * Z * Z) / Phi;
			z = Phi;
	
			Z = 0;
			for (j = 0 ; j < d ; j++) {
				m[ j ] = mOld[ j ] + y[ i ] * lambda * vOld[ j ] * X[ i + st * j ];
				Z += X[ i + st * j ] * m[ j ];
			}
	
			for (j = 0 ; j < d ; j++) {
				v[ j ] = vOld[ j ] - lambda * (y[ i ] * Z + lambda) / (squareRoot * squareRoot) * vOld[ j ] *
					vOld[ j ] * X[ i + st * j ] * X[ i + st * j ];
				if (v[ j ] < 1e-15)
					v[ j ] = 1e-15;
				termLikelihood[ i ].vTilde[ j ] = 1 / (1 / v[ j ] - 1 / vOld[ j ]);
				if (!isinf(termLikelihood[ i ].vTilde[ j ]))
					termLikelihood[ i ].mTilde[ j ] = mOld[ j ] + y[ i ] * lambda * X[ i + st * j ] *
						(termLikelihood[ i ].vTilde[ j ] + vOld[ j ]);
				else
					termLikelihood[ i ].mTilde[ j ] = 0;
			}
	
/*			K1 = eHatOld * (1 - Phi) / (aOld * (eHatOld + (1 - 2 * eHatOld) * Phi)) + digamma(aOld) - digamma(aOld + bOld + 1);
			K2 = (1 - eHatOld) * Phi / (bOld * (eHatOld + (1 - 2 * eHatOld) * Phi)) + digamma(bOld) - digamma(aOld + bOld + 1);
		
			findAnewBnew(K1, K2, &a, &b, R_optimizer, R_rho); */

/*			K1 = 1 / z * (Phi * (1 - eHatOld) * aOld / (aOld + bOld + 1) + (1 - Phi) * eHatOld * (aOld + 1) / (aOld + bOld + 1));
			K2 = 1 / z * (Phi * (1 - eHatOld) * aOld * (aOld + 1) / ((aOld + bOld + 1) * (aOld + bOld + 2)) +
				(1 - Phi) * eHatOld * (aOld + 1) * (aOld + 2) / ((aOld + bOld + 1) * (aOld + bOld + 2)));
	
			a = (K1 - K2) / (K2 - K1 * K1) * K1;
			b = (K1 - K2) / (K2 - K1 * K1) * (1 - K1); 

			termLikelihood[ i ].aTilde = a - aOld;
			termLikelihood[ i ].bTilde = b - bOld; */

			termLikelihood[ i ].sTilde = log(z);
			for (j = 0 ; j < d ; j++) {
				termLikelihood[ i ].sTilde += log(sqrt(1 + vOld[ j ] / termLikelihood[ i ].vTilde[ j ]));
				termLikelihood[ i ].sTilde += 0.5 * (termLikelihood[ i ].mTilde[ j ] - mOld[ j ]) *
					(termLikelihood[ i ].mTilde[ j ] - mOld[ j ]) / (termLikelihood[ i ].vTilde[ j ] + vOld[ j ]);
			}
/*			termLikelihood[ i ].sTilde += lbeta(aOld, bOld) - lbeta(a, b);*/
		}


		/* We process the first terms of the Markov random field prior */

		firstTermMarkov.sTilde = 0;
		for (i = 0 ; i < d ; i++) {
			pOld[ i ] = p[ i ] / firstTermMarkov.cTilde[ i ] / (p[ i ] / firstTermMarkov.cTilde[ i ] +
				(1 - p[ i ]) / firstTermMarkov.dTilde[ i ]);

			p[ i ] = 1 / (1 + (1 / pOld[ i ] - 1) * exp(-2 * alpha));

			firstTermMarkov.cTilde[ i ] = p[ i ] / pOld[ i ];
			firstTermMarkov.dTilde[ i ] = (1 - p[ i ]) / (1 - pOld[ i ]);
			firstTermMarkov.sTilde += log(exp(alpha) * pOld[ i ] + exp(-alpha) * (1 - pOld[ i ]));
		}
	
		/* We process the second terms of the Markov random field prior */

		if (beta != 0) {
			for (k = 0 ; k < sn ; k++) {
				i = indexSecondTermMarkov[ k ];
				j = N[ i ] - 1;
				l = N[ i + 2 * sn ] - 1;
				w = N[ i + sn ];
		
				pOld_j = p[ j ] / secondTermMarkov.c1Tilde[ i ] /
					(p[ j ] / secondTermMarkov.c1Tilde[ i ] + (1 - p[ j ]) / secondTermMarkov.d1Tilde[ i ]);
 				pOld_l = p[ l ] / secondTermMarkov.c2Tilde[ i ] /
					(p[ l ] / secondTermMarkov.c2Tilde[ i ] + (1 - p[ l ]) / secondTermMarkov.d2Tilde[ i ]);
	
				if (pOld_j != 0.5 && pOld_l != 0.5) {
					Z1 = pOld_j * pOld_l * exp(beta * w);
					Z2 = (1 - pOld_j) * (1 - pOld_l) * exp(beta * w);
					Z3 = (1 - pOld_j) * pOld_l * exp(-beta * w);
					Z4 = pOld_j * (1 - pOld_l) * exp(-beta * w);
					p[ j ] = (Z1 + Z4) / (Z1 + Z2 + Z3 + Z4);
					p[ l ] = (Z1 + Z3) / (Z1 + Z2 + Z3 + Z4);
					secondTermMarkov.sTilde[ i ] = log(Z1 + Z2 + Z3 + Z4);
				}
	
				if (p[ j ] < 1e-15)
					p[ j ] = 1e-15;
				if (p[ j ] > 1 - 1e-15)
					p[ j ] = 1 - 1e-15;
					
				if (p[ l ] < 1e-15)
					p[ l ] = 1e-15;
				if (p[ l ] > 1 - 1e-15)
					p[ l ] = 1 - 1e-15;
	
				secondTermMarkov.c1Tilde[ i ] = p[ j ] / pOld_j;
				secondTermMarkov.d1Tilde[ i ] = (1 - p[ j ]) / (1 - pOld_j);
				secondTermMarkov.c2Tilde[ i ] = p[ l ] / pOld_l;
				secondTermMarkov.d2Tilde[ i ] = (1 - p[ l ]) / (1 - pOld_l);
			}
		}
	
		termSpikeAndSlab.sTilde = 0;
		for (i = 0 ; i < d ; i++) {
			pOld[ i ] = p[ i ] / termSpikeAndSlab.cTilde[ i ] /
				(p[ i ] / termSpikeAndSlab.cTilde[ i ] + (1 - p[ i ]) / termSpikeAndSlab.dTilde[ i ]);
			vOld[ i ] = 1 / (1 / v[ i ] - 1 / termSpikeAndSlab.vTilde[ i ]);
			if (isinf(vOld[ i ])) {
				mOld[ i ] = m[ i ] = 0;
				v[ i ] = p[ i ] = pOld[ i ];
				termSpikeAndSlab.vTilde[ i ] = v[ i ];
				termSpikeAndSlab.mTilde[ i ] = 0;
				termSpikeAndSlab.cTilde[ i ] = 1;
				termSpikeAndSlab.dTilde[ i ] = 1;
				} else {
				if (!isinf(vOld[ i ]) && vOld[ i ] > 0) {

					v1 = 1;

					mOld[ i ] = m[ i ] + vOld[ i ] * 1 / termSpikeAndSlab.vTilde[ i ] * (m[ i ] - termSpikeAndSlab.mTilde[ i ]);
					Z = pOld[ i ] * 1 / sqrt(2 * PI * (vOld[ i ] + v1)) * exp(-0.5 * mOld[ i ] * mOld[ i ] /
						(vOld[ i ] + v1)) + (1 - pOld[ i ]) * 1 / sqrt(2 * PI * vOld[ i ]) * exp(-0.5 * mOld[ i ] *
						mOld[ i ] / vOld[ i ]);
					C = 1 / (1 + (1 - pOld[ i ]) / pOld[ i ] * sqrt(vOld[ i ] + v1) / sqrt(vOld[ i ]) *
						exp(-0.5 * mOld[ i ] * mOld[ i ] * (1 / vOld[ i ] - 1 / (vOld[ i ] + v1)))) *
						- mOld[ i ] / (vOld[ i ] + v1) + 1 / (1 + pOld[ i ] / (1 - pOld[ i ]) * sqrt(vOld[ i ]) /
						sqrt(vOld[ i ] + v1) * exp(-0.5 * mOld[ i ] * mOld[ i ] * (1 / (vOld[ i ] + v1) - 1 / vOld[ i ]))) *
							- mOld[ i ] / vOld[ i ];
					Cprima = 1 / (1 + (1 - pOld[ i ]) / pOld[ i ] * sqrt(vOld[ i ] + v1) / sqrt(vOld[ i ]) *
						exp(-0.5 * mOld[ i ] * mOld[ i ] * (1 / (vOld[ i ]) - 1 / (vOld[ i ] + v1)))) *
			                        0.5 * (mOld[ i ] * mOld[ i ] / ((vOld[ i ] + v1) * (vOld[ i ] + v1)) - 1 / (vOld[ i ] + v1)) +
			                        1 / (1 + pOld[ i ] / (1 - pOld[ i ]) * sqrt(vOld[ i ]) / sqrt(vOld[ i ] + v1) * exp(-0.5 * mOld[ i ] *
							mOld[ i ] * (1 / (vOld[ i ] + v1) - 1 / vOld[ i ]))) *  0.5 * (mOld[ i ] * mOld[ i ] / (vOld[ i ] *
						vOld[ i ]) - 1 / vOld[ i ]);
					Cprimaprima = C * C - 2 * Cprima;
					v[ i ] = vOld[ i ] - Cprimaprima * vOld[ i ] * vOld[ i ];
					m[ i ] = mOld[ i ] + C * vOld[ i ];
					p[ i ] = 1 / (1 + (1 - pOld[ i ]) / pOld[ i ] * sqrt(vOld[ i ] + v1) / sqrt(vOld[ i ]) *
							exp(-0.5 * mOld[ i ] * mOld[ i ] * (1 / vOld[ i ] - 1 / (vOld[ i ] + v1))));

					if (p[ i ] < 1e-15)
						p[ i ] = 1e-15;
					if (p[ i ] > 1 - 1e-15)
						p[ i ] = 1 - 1e-15;
					if (v[ i ] < 1e-15)
							v[ i ] = 1e-15;

					termSpikeAndSlab.vTilde[ i ] = 1 / Cprimaprima - vOld[ i ];
					termSpikeAndSlab.mTilde[ i ] = mOld[ i ] + C * (termSpikeAndSlab.vTilde[ i ] + vOld[ i ]);
					termSpikeAndSlab.cTilde[ i ] = p[ i ] / pOld[ i ];
					termSpikeAndSlab.dTilde[ i ] = (1 - p[ i ]) / (1 - pOld[ i ]);

					termSpikeAndSlab.sTilde += log(Z) + log(sqrt(1 + vOld[ i ] / termSpikeAndSlab.vTilde[ i ])) +
						0.5 * C * C / Cprimaprima;
				} else {
					/* printf("#"); fflush(stdout); */
				}
			}
		}
	
	
		/* We check for convergence */

		iterations++;

		if (iterations > 1) {
			maxdif = 0;
			for (i = 0 ; i < d ; i++) {
				maxdif = max(internalAbsoluteValue(m[ i ] - mBackup[ i ]), maxdif);
				maxdif = max(internalAbsoluteValue(v[ i ] - vBackup[ i ]), maxdif);
				maxdif = max(internalAbsoluteValue(p[ i ] - pBackup[ i ]), maxdif);
			}
/*
			maxdif = max(internalAbsoluteValue(a - aBackup), maxdif);

			maxdif = max(internalAbsoluteValue(b - bBackup), maxdif);
*/

			 printf("Iteration %d. - %f - %f - %f\n", iterations, maxdif, a, b); fflush(stdout); 

			if (maxdif < 1e-5)
				convergence = 1;

		}

		/* We copy the current parameter estimates */

		for (i = 0 ; i < d ; i++) {
			mBackup[ i ] = m[ i ];
			vBackup[ i ] = v[ i ];
			pBackup[ i ] = p[ i ];
		}
/*		aBackup = a;
		bBackup = b;*/
	}

	/* We evaluate the logEvidence */

	logEvidence = firstTermMarkov.sTilde;

	for (i = 0 ; i < sn ; i++)
		logEvidence += secondTermMarkov.sTilde[ i ];

	logEvidence += termSpikeAndSlab.sTilde;
	logEvidence += termPriorEpsilon.sTilde;

	for (i = 0 ; i < st ; i++)
		logEvidence += termLikelihood[ i ].sTilde;

	A = 1;
	for (i = 0 ; i < st ; i++)
		A += termLikelihood[ i ].aTilde;
	A += termPriorEpsilon.aTilde;

	B = 1;
	for (i = 0 ; i < st ; i++)
		B += termLikelihood[ i ].bTilde;
	B += termPriorEpsilon.bTilde;

	D = 0;
	for (i = 0 ; i < d ; i++)
		D += m[ i ] * m[ i ] / v[ i ];

	for (j = 0 ; j < st ; j++)
		for (i = 0 ; i < d ; i++) {
			D -= termLikelihood[ j ].mTilde[ i ] * termLikelihood[ j ].mTilde[ i ] / termLikelihood[ j ].vTilde[ i ];
		}

	for (i = 0 ; i < d ; i++)
		D -= termSpikeAndSlab.mTilde[ i ] * termSpikeAndSlab.mTilde[ i ] / termSpikeAndSlab.vTilde[ i ];
	
	logEvidence += lbeta(A, B) + D / 2 + d / 2 * log(2 * PI);
	
	for (i = 0 ; i < d ; i++)
		logEvidence += 0.5 * log(v[ i ]);

	cProduct = (double *) malloc(sizeof(double) * d);
	dProduct = (double *) malloc(sizeof(double) * d);

	for (i = 0 ; i < d ; i++) {
		cProduct[ i ] = firstTermMarkov.cTilde[ i ] * termSpikeAndSlab.cTilde[ i ];
		dProduct[ i ] = firstTermMarkov.dTilde[ i ] * termSpikeAndSlab.dTilde[ i ];
	}

	for (i = 0 ; i < sn ; i++) {
		j = N[ i ] - 1;
		l = N[ i + 2 * sn ] - 1;
		cProduct[ j ] *= secondTermMarkov.c1Tilde[ i ];
		dProduct[ j ] *= secondTermMarkov.d1Tilde[ i ];
		cProduct[ l ] *= secondTermMarkov.c2Tilde[ i ];
		dProduct[ l ] *= secondTermMarkov.d2Tilde[ i ];
	}

	C = 0;
	for (i = 0 ; i < d ; i++)
		C += log(cProduct[ i ] + dProduct[ i ]);
	logEvidence += C;

	/* We evaluate the partition funciton */

	logEvidence -= approximatePartitionFunctionEP(d, N, sn, alpha, beta, R_runif, R_rho);

	free(dProduct); free(cProduct);

	free(mOld); free(vOld); free(pOld);
	free(mBackup); free(vBackup); free(pBackup);

	free(firstTermMarkov.cTilde); free(firstTermMarkov.dTilde);
	free(secondTermMarkov.c1Tilde); free(secondTermMarkov.d1Tilde);
	free(secondTermMarkov.c2Tilde); free(secondTermMarkov.d2Tilde);
	free(termSpikeAndSlab.cTilde); free(termSpikeAndSlab.dTilde);
	free(termSpikeAndSlab.mTilde); free(termSpikeAndSlab.vTilde);

	for (i = 0 ; i < st ; i++) {
		free(termLikelihood[ i ].mTilde);
		free(termLikelihood[ i ].vTilde);
	}
	free(termLikelihood);

	*REAL(R_a) = a;
	*REAL(R_b) = b;
	*REAL(R_logEvidence) = logEvidence;
	*REAL(R_error) = a / (a + b);

	if (convergence != 1) {
		*REAL(R_logEvidence) = -HUGE_VAL;
		*REAL(R_error) = 1;
	}

	SET_VECTOR_ELT(R_ret, 0, R_m);
	SET_VECTOR_ELT(R_ret, 1, R_v);
	SET_VECTOR_ELT(R_ret, 2, R_p);
	SET_VECTOR_ELT(R_ret, 3, R_a);
	SET_VECTOR_ELT(R_ret, 4, R_b);
	SET_VECTOR_ELT(R_ret, 5, R_logEvidence);
	SET_VECTOR_ELT(R_ret, 6, R_error);

	PROTECT(R_namesList = allocVector(STRSXP, 7));

        SET_STRING_ELT(R_namesList, 0, mkChar("m"));
        SET_STRING_ELT(R_namesList, 1, mkChar("v"));
        SET_STRING_ELT(R_namesList, 2, mkChar("p"));
        SET_STRING_ELT(R_namesList, 3, mkChar("a"));
        SET_STRING_ELT(R_namesList, 4, mkChar("b"));
        SET_STRING_ELT(R_namesList, 5, mkChar("logEvidence"));
        SET_STRING_ELT(R_namesList, 6, mkChar("error"));
        setAttrib(R_ret, R_NamesSymbol, R_namesList);

	UNPROTECT(19);

	return R_ret;
}

/**
 * Function that approximates the partition function of a Markov Random field model
 */

double approximatePartitionFunctionEP(int d, double *N, int sn, double alpha, double beta, SEXP R_runif, SEXP R_rho) {

	SEXP R_p;

	double Z1, Z2, Z3, Z4, w, pOld_j, pOld_l, maxdif, logEvidence, C;

	double *pBackup, *pOld, *p, *cProduct, *dProduct;

	int i, j, k, l, iterations, convergence;

	int *indexSecondTermMarkov;

	FirstTermMarkov firstTermMarkov;
	SecondTermMarkov secondTermMarkov;

	p = (double *) malloc(sizeof(double) * d);
	pBackup = (double *) malloc(sizeof(double) * d);
	pOld = (double *) malloc(sizeof(double) * d);

	/* We initialize the approximate term for the first d terms of the Markov random field prior */

	firstTermMarkov.cTilde = (double *) malloc(sizeof(double) * d);
	firstTermMarkov.dTilde = (double *) malloc(sizeof(double) * d);
	firstTermMarkov.sTilde = 0;
	for (i = 0 ; i < d ; i++) {
		firstTermMarkov.cTilde[ i ] = 1;
		firstTermMarkov.dTilde[ i ] = 1;
	}

	/* We initialize the approximate term for the second term of the Markov random field prior */

	secondTermMarkov.c1Tilde = (double *) malloc(sizeof(double) * sn);
	secondTermMarkov.d1Tilde = (double *) malloc(sizeof(double) * sn);
	secondTermMarkov.c2Tilde = (double *) malloc(sizeof(double) * sn);
	secondTermMarkov.d2Tilde = (double *) malloc(sizeof(double) * sn);
	secondTermMarkov.sTilde = (double *) malloc(sizeof(double) * sn);
	for (i = 0 ; i < sn ; i++) {
		secondTermMarkov.c1Tilde[ i ] = 1;
		secondTermMarkov.d1Tilde[ i ] = 1;
		secondTermMarkov.c2Tilde[ i ] = 1;
		secondTermMarkov.d2Tilde[ i ] = 1;
		secondTermMarkov.sTilde[ i ] = 0;
	}

	/* We initialize the posterior approximation to the priors */

	for (i = 0 ; i < d ; i++)
		p[ i ] = 0.5;

	indexSecondTermMarkov = computePermutation(sn, R_runif, R_rho);

	/* Main loop of the algorithm */

	convergence = 0;
	iterations = 0;
	while (!convergence && iterations < 500) {

		/* We process the first terms of the Markov random field prior */

		firstTermMarkov.sTilde = 0;
		for (i = 0 ; i < d ; i++) {
			pOld[ i ] = p[ i ] / firstTermMarkov.cTilde[ i ] / (p[ i ] / firstTermMarkov.cTilde[ i ] +
				(1 - p[ i ]) / firstTermMarkov.dTilde[ i ]);
			p[ i ] = 1 / (1 + (1 / pOld[ i ] - 1) * exp(-2 * alpha));

			firstTermMarkov.cTilde[ i ] = p[ i ] / pOld[ i ];
			firstTermMarkov.dTilde[ i ] = (1 - p[ i ]) / (1 - pOld[ i ]);
			firstTermMarkov.sTilde += log(exp(alpha) * pOld[ i ] + exp(-alpha) * (1 - pOld[ i ]));
		}
	
		/* We process the second terms of the Markov random field prior */

		if (beta != 0) {
			printf("Processing Markov Random Field!\n");
			for (k = 0 ; k < sn ; k++) {
				i = indexSecondTermMarkov[ k ];
				j = N[ i ] - 1;
				l = N[ i + 2 * sn ] - 1;
				w = N[ i + sn ];
	
				pOld_j = p[ j ] / secondTermMarkov.c1Tilde[ i ] /
					(p[ j ] /secondTermMarkov.c1Tilde[ i ] + (1 - p[ j ]) / secondTermMarkov.d1Tilde[ i ]);
					pOld_l = p[ l ] / secondTermMarkov.c2Tilde[ i ] /
					(p[ l ] / secondTermMarkov.c2Tilde[ i ] + (1 - p[ l ]) / secondTermMarkov.d2Tilde[ i ]);
	
				Z1 = pOld_j * pOld_l * exp(beta * w);
				Z2 = (1 - pOld_j) * (1 - pOld_l) * exp(beta * w);
				Z3 = (1 - pOld_j) * pOld_l * exp(-beta * w);
				Z4 = pOld_j * (1 - pOld_l) * exp(-beta * w);
				p[ j ] = (Z1 + Z4) / (Z1 + Z2 + Z3 + Z4);
				p[ l ] = (Z1 + Z3) / (Z1 + Z2 + Z3 + Z4);
				secondTermMarkov.sTilde[ i ] = log(Z1 + Z2 + Z3 + Z4);

				if (p[ j ] < 1e-15)
					p[ j ] = 1e-15;
				if (p[ j ] > 1 - 1e-15)
					p[ j ] = 1 - 1e-15;
					
				if (p[ l ] < 1e-15)
					p[ l ] = 1e-15;
				if (p[ l ] > 1 - 1e-15)
					p[ l ] = 1 - 1e-15;
	
				secondTermMarkov.c1Tilde[ i ] = p[ j ] / pOld_j;
				secondTermMarkov.d1Tilde[ i ] = (1 - p[ j ]) / (1 - pOld_j);
				secondTermMarkov.c2Tilde[ i ] = p[ l ] / pOld_l;
				secondTermMarkov.d2Tilde[ i ] = (1 - p[ l ]) / (1 - pOld_l);
			}
		}
	
		/* We check for convergence */

		iterations++;

		if (iterations > 1) {
			maxdif = 0;
			for (i = 0 ; i < d ; i++)
				maxdif = max(internalAbsoluteValue(p[ i ] - pBackup[ i ]), maxdif);

			if (maxdif < 1e-5)
				convergence = 1;
		}

		/* We copy the current parameter estimates */

		for (i = 0 ; i < d ; i++)
			pBackup[ i ] = p[ i ];
	}

	/* We evaluate the logEvidence */

	logEvidence = firstTermMarkov.sTilde;

	for (i = 0 ; i < sn ; i++)
		logEvidence += secondTermMarkov.sTilde[ i ];

	cProduct = (double *) malloc(sizeof(double) * d);
	dProduct = (double *) malloc(sizeof(double) * d);

	for (i = 0 ; i < d ; i++) {
		cProduct[ i ] = firstTermMarkov.cTilde[ i ];
		dProduct[ i ] = firstTermMarkov.dTilde[ i ];
	}

	for (i = 0 ; i < sn ; i++) {
		j = N[ i ] - 1;
		l = N[ i + 2 * sn ] - 1;
		cProduct[ j ] *= secondTermMarkov.c1Tilde[ i ];
		dProduct[ j ] *= secondTermMarkov.d1Tilde[ i ];
		cProduct[ l ] *= secondTermMarkov.c2Tilde[ i ];
		dProduct[ l ] *= secondTermMarkov.d2Tilde[ i ];
	}

	C = 0;
	for (i = 0 ; i < d ; i++)
		C += log(cProduct[ i ] + dProduct[ i ]);
	logEvidence += C;

	free(dProduct); free(cProduct);
	free(pOld); free(pBackup); free(p);

	free(firstTermMarkov.cTilde); free(firstTermMarkov.dTilde);
	free(secondTermMarkov.c1Tilde); free(secondTermMarkov.d1Tilde);
	free(secondTermMarkov.c2Tilde); free(secondTermMarkov.d2Tilde);

	return logEvidence;
}
